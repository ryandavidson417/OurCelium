using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : Terrain
{

    //[SerializeField] public Vector2 position { get; set; }
    protected override void onClick()
    {
        
    }

    [SerializeField] GameObject highlight;

    void OnMouseEnter()
    {
        highlight.SetActive(true);
    }

    void OnMouseExit()
    {
        highlight.SetActive(false);
    }
}
