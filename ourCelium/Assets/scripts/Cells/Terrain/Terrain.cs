using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Terrain : Cell
{
    protected int cost;
    protected bool traversible;

    public int Cost { get { return cost; } private set { cost = value; } }
}
