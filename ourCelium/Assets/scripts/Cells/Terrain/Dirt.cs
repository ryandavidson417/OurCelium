using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirt : Terrain
{
    //[SerializeField] public Vector2 position { get; set; }


    protected override void onClick()
    {
        
    }


    // Start is called before the first frame update
    public override void Start()
    {
        gridManager = GridManager._instance;
        gameManager = GameManager._instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [SerializeField] GameObject highlight;

    void OnMouseEnter()
    {
        highlight.SetActive(true);
    }

    void OnMouseExit()
    {
        highlight.SetActive(false);
    }

    void OnMouseOver()
    {
        if (adjToFungus && gameManager.gameState == GameManager.GameState.Playing)
        {
            if(Input.GetMouseButtonDown(0))
            {
                gridManager.changeType(this, GridManager.CellType.hyphae);
                //scoremanager.subscore(hyphae.cost)
            } 
            if(Input.GetMouseButtonDown(1))
            {
                gridManager.changeType(this, GridManager.CellType.mushroom);
                //scoremanager.subscore(hyphae.cost)
            }
        }
    }

}
