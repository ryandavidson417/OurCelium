using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public abstract class Cell : MonoBehaviour
{
    [SerializeField] public Vector2Int position;
    protected abstract void onClick();
    public bool visible;
    public bool adjToFungus;

    protected GridManager gridManager;
    protected GameManager gameManager;

    virtual public void Start()
    {
        gridManager = GridManager._instance;
        gameManager = GameManager._instance;
    }

    //old stuff
    //[SerializeField] private Sprite emptySprite;
    //[SerializeField] private Sprite rockSprite;

    //[SerializeField] private SpriteRenderer spriteRenderer;

    //[SerializeField] public enum State
    //{
    //    empty,
    //    rock
    //}

    //public State state;

    //private void Awake()
    //{

    //    int rand = Random.Range(0, 100);

    //    switch (rand)//starting out with 50-50 chance for rock or empty. can alter ratios and add states when we design other cell types
    //    {
    //        case int n when n < 50: //when rand is less than 50
    //            state = State.rock;
    //            spriteRenderer.sprite = rockSprite;
    //            break;
    //        case int n when n >= 50: //when rand is greater than or equal to 50
    //            state = State.empty;
    //            spriteRenderer.sprite = emptySprite;
    //            break;
    //    }
    //}

    //// Start is called before the first frame update
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}


    //public void setState(State state)
    //{
    //    Debug.Log("setState " + state);

    //    this.state = state;

    //    switch (state)
    //    {
    //        case State.rock:
    //            spriteRenderer.sprite = rockSprite;
    //            break;
    //        case State.empty:
    //            spriteRenderer.sprite = emptySprite;
    //            break;

    //    }
    //}
}
