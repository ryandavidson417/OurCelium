using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeerHead : Food
{
    [SerializeField] Sprite hyphaeSprite;
    [SerializeField] SpriteRenderer spriteRenderer;

    protected int foodVal = 5;

    

    protected override void onClick()
    {
        
    }

    private void OnMouseOver()
    {
        if (adjToFungus && gameManager.gameState == GameManager.GameState.Playing)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ScoreManager._instance.AddPoint(foodVal);
                gridManager.changeType(this, GridManager.CellType.feeder);

            }
            if (Input.GetMouseButtonDown(1))
            {
                ScoreManager._instance.AddPoint(foodVal);
                gridManager.changeType(this, GridManager.CellType.mushroom);
            }
        }
    }
}
