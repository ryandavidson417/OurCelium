using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Fungus : Cell
{
    [Tooltip("the default value of how much each fungus takes to place. This only applies to mushrooms, as feeders and hyphae have their cost overridden by DFM")]
    public int cost;
    
    [Tooltip("the radius of the light field projected by this fungus, in number of tiles from the center ")]
    [SerializeField] private float lightPoolRadius;
    public float LightPoolRadius { get { return lightPoolRadius; } }


    [Tooltip("DistanceFromMushroom - for all hyphae (and probably feeders) this will be equivalent to cost")]
    public int DFM;


    public override void Start()
    {

        gridManager = GridManager._instance;
        gameManager = GameManager._instance;
        //findDFM();
        findDistanceFromMushroom();
        if (!gridManager.init)
        {
            ScoreManager._instance.SubPoints(cost);
            
        }
        //exploreAdjacentTiles();
    }

    virtual protected void exploreAdjacentTiles()
    {
        List<Cell> adjacent = gridManager.findAdjacent(position, false);

        foreach (Cell cell in adjacent)
        {
            cell.adjToFungus = true;
            cell.visible = true;
            //consider a recursive call to cell.exploreAdjTiles to set visible to true if(adjToFungus), this would allow visible to be true across two adjacent tiles

        }

    }

    virtual public void findDistanceFromMushroom()
    {
        List<Cell> adjacent = gridManager.findAdjacent(position, false);
        List<Fungus> fungi = new List<Fungus>();
        int currLow = int.MaxValue;
        foreach (Cell cell in adjacent)
        {
            if( cell.TryGetComponent<Fungus>(out Fungus fungus)){
                if (fungus.TryGetComponent < Feeder>(out Feeder feeder))//if it's a feeder then we probably don't want to subtract any 
                {

                }
                {

                }
                if (currLow > fungus.DFM)
                {
                    currLow = fungus.DFM;
                }
                fungi.Add(fungus);
            } else {
                cell.adjToFungus = true;
                cell.visible = true;
                //TODO:
                //if(cell.TryGetComponent<Terrain>( out Terrain terrain))
                //{
                    //consider adding a call to terrain.exploreAdjacentCells, which will set all adjacent cells to visible
                //} 
            }

        }
        if (currLow == int.MaxValue)
        {
            Debug.Log("you somehow managed to call findDFM on an object with no adjacent fungi? or maybe you're improperly comparing the costs");
        }
        DFM = currLow + 1;
        cost = DFM;
        foreach (Fungus fungus in fungi)
        {
            if(this.DFM < fungus.DFM)
            {
                fungus.findDistanceFromMushroom();

            }
        }
    }


}
