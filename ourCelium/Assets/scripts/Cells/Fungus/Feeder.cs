using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feeder : Fungus
{


    public override void Start()
    {
        gridManager = GridManager._instance;
        findDistanceFromMushroom();
        if (!gridManager.init)
        {
            ScoreManager._instance.SubPoints(cost);
        }
        //current plan amongst game designers is to not subtract any points - though this can be tweaked with relative eas
        //ScoreManager.instance.SubPoints(cost);
    }


    protected override void onClick()
    {
        
    }

/*
    protected override void onPlace() //we'll have to add these functions to fungus.cs
    {
        
    }

    protected override void recurseDFM()
    {
        
    }

    protected override void setDFM()
    {
        
    }*/
}
