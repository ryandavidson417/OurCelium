using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : Fungus
{
    private bool init;
    public void Awake()
    {
        gridManager = GridManager._instance;

        if (gridManager.init)
        {
            this.init = true;
        }
    }


    public override void Start()
    {
        findDistanceFromMushroom();
        exploreAdjacentTiles();
        if (!this.init)
        {
            ScoreManager._instance.SubPoints(cost);
        }
        DFM = 0;
    }

    protected override void onClick()
    {
        
    }

    public override void findDistanceFromMushroom()
    {
        List<Cell> adjacent = gridManager.findAdjacent(position, false);
        foreach (Cell cell in adjacent)
        {
            if (cell.TryGetComponent<Fungus>(out Fungus fungus))
            {
                if(fungus.DFM > 1)
                {
                    fungus.findDistanceFromMushroom();
                }
            }
            else
            {
                cell.adjToFungus = true;
                cell.visible = true;
                //TODO:
                //if(cell.TryGetComponent<Terrain>( out Terrain terrain))
                //{
                //consider adding a call to terrain.exploreAdjacentCells, which will set all adjacent cells to visible
                //} see if we need to do anything specific with feeder
            }

        }
        return;
        //I don't think we want to do anything here - just end the recursive loop that hyphae will be calling
    }

/*
    protected override void onPlace()
    {
        
    }

    protected override void recurseDFM()
    {
        
    }

    protected override void setDFM()
    {
        
    }*/
}
