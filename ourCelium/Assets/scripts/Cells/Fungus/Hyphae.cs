using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hyphae : Fungus
{

    [SerializeField] private Texture2D lightTexture;

    public override void Start()
    {

        gridManager = GridManager._instance;
        //findDFM();
        findDistanceFromMushroom();
        if (!gridManager.init)
        {
            ScoreManager._instance.SubPoints(cost);
        }

        //spriteMask = this.GetComponent<SpriteMask>();

        //maskSprite = Sprite.Create(lightTexture, new Rect(0, 0, lightTexture.height, lightTexture.width),
        //                            new Vector2(.5f,.5f), 256, 256u, SpriteMeshType.FullRect, new Vector4(1024, 1024, 1024, 1024));

        ////spriteMask.transform.localScale += new Vector3(2,2,2);

        //maskSprite.name = "maskSprite";
        //Debug.Log(maskSprite.bounds);

        //spriteMask.sprite = maskSprite;

        //Texture2D texture = maskSprite.texture;

        //Debug.Log("pivot 1 " + maskSprite.pivot);

        //newMask = Sprite.Create(texture, new Rect(0,0, texture.height, texture.width), new Vector2(maskSprite.pivot.x - maskSprite.rect.x, maskSprite.pivot.y - maskSprite.rect.y), 32f);

        //Debug.Log("pivot 2 " + newMask.pivot);


        //spriteMask.sprite = newMask;

        //exploreAdjacentTiles();
    }

    private void OnGUI()
    {

        //spriteMask.sprite = maskSprite;
    }

    protected override void onClick()
    {
        
    }

    override protected void exploreAdjacentTiles()
    {
        List<Cell> adjacent = gridManager.findAdjacent(position, false);

        foreach(Cell cell in adjacent){
            cell.adjToFungus = true;
            cell.visible = true;
            //consider a recursive call to cell.exploreAdjTiles to set visible to true if(adjToFungus), this would allow visible to be true across two adjacent tiles

        }
        
    }

/*
    public void findDistanceFromMushroom()
    {
        List<Cell> adjacent = 
    }*/

/*
    protected override void onPlace()
    {
        
    }

    protected override void recurseDFM()
    {
        
    }

    protected override void setDFM()
    {
        
    }*/
}
