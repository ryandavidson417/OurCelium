using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class PauseMenuController : MonoBehaviour
{

    [SerializeField] private UIDocument UIDoc;
    private VisualElement root;
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button quitButton;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager._instance;

        root = UIDoc.rootVisualElement;
        root.visible = false;

        resumeButton = root.Q<Button>("ResumeButton");
        resumeButton.clicked += pauseResumeFunction;
        quitButton = root.Q<Button>("QuitButton");
        quitButton.clicked += quitFunction;
        restartButton = root.Q<Button>("RestartButton");
        restartButton.clicked += restartFunction;

        gameManager.gamePauseEvent.AddListener(pauseResumeFunction);
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)) && gameManager.gameState != GameManager.GameState.Dead)
        {
            //pauseResumeFunction();
            gameManager.gamePauseEvent.Invoke();
        }

    }

    private void pauseResumeFunction()
    {
        if (gameManager.gameState == GameManager.GameState.Paused)
        {
            root.visible = false;
            gameManager.gameState = GameManager.GameState.Playing;
        }
        else
        {
            root.visible = true;
            gameManager.gameState = GameManager.GameState.Paused;
        }
    }

    private void quitFunction()
    {
        Application.Quit();
    }

    private void restartFunction()
    {
        root.visible = false;
        gameManager.resetGame();
    }
}
