using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class DeathMenuController : MonoBehaviour
{
    [SerializeField] private UIDocument UIDoc;
    private VisualElement root;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button menuButton;
    [SerializeField] private Button quitButton;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager._instance;

        root = UIDoc.rootVisualElement;
        root.visible = false;

        restartButton = root.Q<Button>("RestartButton");
        restartButton.clicked += restartFunction;
        menuButton = root.Q<Button>("MainMenuButton");
        menuButton.clicked += pauseResumeFunction;
        quitButton = root.Q<Button>("QuitButton");
        quitButton.clicked += quitFunction;

        gameManager.deathEvent.AddListener(deathFunction);

    }

    private void deathFunction()
    {
        root.visible = true;
    }

    private void restartFunction()
    {
        root.visible = false;
        gameManager.resetGame();
    }

    private void pauseResumeFunction()
    {
        Debug.Log("you need to implement the main menu and load it when you press the menu button");
        throw new NotImplementedException();
    }

    private void quitFunction()
    {
        Application.Quit();
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
