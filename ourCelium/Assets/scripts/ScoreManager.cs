using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class ScoreManager : MonoBehaviour
{
    public TMP_Text scoreText;
    public static ScoreManager _instance;

    [SerializeField] private int startingScore;
    int score = 50;

    // Start is called before the first frame update
    void Start()
    {
        score = startingScore;
        scoreText.text = score.ToString() + " ENERGY";
    }

    private void Awake()
    {

        //is this the first time we've created this singleton
        if (_instance == null)
        {
            //we're the first GameManager, so assign ourselves to this instance
            _instance = this;
        }
        else
        {
            //if there's another one, then destroy this one
            Destroy(this.gameObject);
        }
    }

    public void resetScore()
    {
        score = startingScore;
        scoreText.text = score.ToString() + " ENERGY";
    }

    public void SubPoints(int points)
    {
        score -= points;
        if(score < 0)
        {
            score = 0;
            GameManager._instance.deathEvent.Invoke();
        }
        scoreText.text = score.ToString() + " ENERGY";
    }

    public void AddPoint(int x)
    {
        score += x;
        scoreText.text = score.ToString() + " ENERGY";
    }
}
