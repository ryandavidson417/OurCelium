using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GridManager : MonoBehaviour
{



    public static GridManager _instance;
    public static GridManager Instance { get { return _instance; } }

    public bool init = true;

    [SerializeField] private static int numRows = 25;
    [SerializeField] private static int numCols = 25;

    [SerializeField] private GameObject dirtPrefab;
    [SerializeField] private GameObject rockPrefab;
    [SerializeField] private GameObject mushPrefab;
    [SerializeField] private GameObject hyphaePrefab;
    [SerializeField] private GameObject deerHeadPrefab;
    [SerializeField] private GameObject feederPrefab;

    [SerializeField] public Transform cam; 

    Cell[,] grid = new Cell[numRows, numCols];

    public enum CellType
    {
        dirt,
        rock,
        deerHead,
        feeder,
        hyphae,
        mushroom
    }

    private void Awake()
    {
        //is this the first time we've created this singleton
        if (_instance == null)
        {
            //we're the first gridManager, so assign ourselves to this instance
            _instance = this;
        }
        else
        {
            //if there's another one, then destroy this one
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        generateGrid();
        init = false;
    }

    public void generateGrid()
    {
        for (int x = 0; x < numRows; x++)
        {
            for (int y = 0; y < numCols; y++)
            {
                //consider a cheeky lil debug line

                //Cell cell = Instantiate(Resources.Load("Prefabs/Cell", typeof(Cell)), new Vector2(x, y), Quaternion.identity) as Cell;

                //consider doing this as a more proper method *LATER*
                Cell cell = chooseType(x, y);


                //grid[x, y].position.Set(x, y);

                if((x==Mathf.Floor(numRows/2)) && (y==Mathf.Floor(numCols/2)))
                {
                    //if in the middle change state to mushroom
                    changeType(grid[x, y], CellType.mushroom);

                }

            }
        }
        cam.transform.position = new Vector3((float)numRows / 2, (float)numCols / 2, -10);

    }

    public void resetGrid()
    {
        init = true;
        foreach(Cell cell in grid)
        {
            DestroyImmediate(cell.gameObject);
        }
        grid = new Cell[numRows, numCols];
        generateGrid();
        init = false;
    }


    /*public void redrawGrid()
    {
        foreach()

        foreach(Cell cell in grid){

        }
    }*/


    private Cell chooseType(int x, int y)
    {
        int rand = Random.Range(0, 100);

        if (rand < 10)//food
        {

            GameObject rockObject = Instantiate(deerHeadPrefab, new Vector2(x, y), Quaternion.identity, this.transform);
            DeerHead deerHead = rockObject.GetComponent<DeerHead>();

            grid[x, y] = deerHead;
            deerHead.position = new Vector2Int(x, y);

            return deerHead;
            //break;
        } else if ( rand < 20) //rocks
        {
            GameObject rockObject = Instantiate(rockPrefab, new Vector2(x, y), Quaternion.identity, this.transform);
            Rock rock = rockObject.GetComponent<Rock>();

            grid[x, y] = rock;
            rock.position = new Vector2Int(x, y);

            return rock;

        } else // dirt
        {

            GameObject dirtObject = Instantiate(dirtPrefab, new Vector2(x, y), Quaternion.identity, this.transform);
            Dirt dirt = dirtObject.GetComponent<Dirt>();

            grid[x, y] = dirt;
            dirt.position = new Vector2Int(x, y);

            return dirt;
        }
/*
        switch (rand)
        {
            

            default:
            case int n when n < 80: //if rand <50 make 
                GameObject dirtObject = Instantiate(dirtPrefab, new Vector2(x, y), Quaternion.identity, this.transform);
                Dirt dirt = dirtObject.GetComponent<Dirt>();

                grid[x, y] = dirt;
                dirt.position = new Vector2(x, y);

                return dirt;
                break;

            case int n when n < 10:
                GameObject rockObject = Instantiate(rockPrefab, new Vector2(x, y), Quaternion.identity, this.transform);
                Rock rock = rockObject.GetComponent<Rock>();

                grid[x, y] = rock;
                rock.position = new Vector2(x, y);

                return rock;
                break;
            case int n when (( n >= 10) && ():
                GameObject rockObject = Instantiate(rockPrefab, new Vector2(x, y), Quaternion.identity, this.transform);
                Rock rock = rockObject.GetComponent<Rock>();

                grid[x, y] = rock;
                rock.position = new Vector2(x, y);

                return rock;
                break;


        }*/

    }


    public void changeType(Cell cell, CellType cellType) //consider passing an enum rather than a string
    {//should this return Cell maybe?

        Vector2Int newPos = cell.position;

        switch (cellType)
        {
            default:
                Debug.Log("you seem to have called changeType with a type that you haven't fully implemented yet");
                break;
            case CellType.dirt:
                GameObject dirtObject = Instantiate(dirtPrefab, new Vector2(newPos.x, newPos.y), Quaternion.identity, this.transform);
                Dirt dirt = dirtObject.GetComponent<Dirt>();
                DestroyImmediate(cell.gameObject);
                dirt.position.Set(newPos.x, newPos.y);
                grid[(int)newPos.x, (int)newPos.y] = dirt;
                break;

            case CellType.rock:
                GameObject rockObject = Instantiate(rockPrefab, new Vector2(newPos.x, newPos.y), Quaternion.identity, this.transform);
                Rock rock = rockObject.GetComponent<Rock>();
                DestroyImmediate(cell.gameObject);
                rock.position.Set(newPos.x, newPos.y);
                grid[(int)newPos.x, (int)newPos.y] = rock;
                break;

            case CellType.hyphae:
                GameObject hyphaeObject = Instantiate(hyphaePrefab, new Vector2(newPos.x, newPos.y), Quaternion.identity, this.transform);
                Hyphae hyphae = hyphaeObject.GetComponent<Hyphae>();
                DestroyImmediate(cell.gameObject);
                hyphae.position.Set(newPos.x, newPos.y);
                grid[(int)newPos.x, (int)newPos.y] = hyphae;
                //hyphae.findDistanceFromMushroom();
                break;

            case CellType.mushroom:
                GameObject mushroomObject = Instantiate(mushPrefab, new Vector2(newPos.x, newPos.y), Quaternion.identity, this.transform);
                Mushroom mushroom = mushroomObject.GetComponent<Mushroom>();
                DestroyImmediate(cell.gameObject);
                mushroom.position.Set(newPos.x, newPos.y);
                grid[(int)newPos.x, (int)newPos.y] = mushroom;
                break;

            case CellType.deerHead:
                GameObject deerHeadObject = Instantiate(deerHeadPrefab, new Vector2(newPos.x, newPos.y), Quaternion.identity, this.transform);
                DeerHead deerHead = deerHeadObject.GetComponent<DeerHead>();
                DestroyImmediate(cell.gameObject);
                deerHead.position.Set(newPos.x, newPos.y);
                grid[(int)newPos.x, (int)newPos.y] = deerHead;
                break;

            case CellType.feeder: //consider scaling this as an enum between different types of Feeders
                GameObject feederObject = Instantiate(feederPrefab, new Vector2(newPos.x, newPos.y), Quaternion.identity, this.transform);
                Feeder feeder = feederObject.GetComponent<Feeder>();
                //if you're scaling this, then pull cell.FoodType into feeder.newType - set these up as enums within feeder and food
                DestroyImmediate(cell.gameObject);
                feeder.position.Set(newPos.x, newPos.y);
                grid[(int)newPos.x, (int)newPos.y] = feeder;
                break;


        }

    }

    /// <summary>
    /// returns a list of the adjacent cells, order goes left to right then top down (like a book)
    /// </summary>
    /// <param name="startingPosition"></param>
    /// <param name="includeCorners"></param>
    /// <returns></returns>
    [Tooltip("returns a list of the adjacent cells, order goes left to right then top down (like a book)")]
    public List<Cell> findAdjacent(Vector2Int startingPosition, bool includeCorners)
    {
        List<Cell> adjacent = new List<Cell>();

        //later: check that we're properly understanding the correlation between rows/colums and x y by running this code with nonequal numcols and numrows
        if(startingPosition.y < numRows - 1)//starting with the top row
        {
            if (includeCorners && startingPosition.x > 0)
            {
                adjacent.Add(grid[startingPosition.x - 1, startingPosition.y + 1]); //top right
            }
            adjacent.Add(grid[startingPosition.x, startingPosition.y + 1]); //top middle
            if (includeCorners && startingPosition.x < numCols -1)
            {
                adjacent.Add(grid[startingPosition.x + 1, startingPosition.y + 1]); //top right
            }
        }//then the middle row (if statement unecessary, solely for nicer formatting/indentation
        {
            if (startingPosition.x > 0)
            {
                adjacent.Add(grid[startingPosition.x - 1, startingPosition.y]); //middle left
            }
            //middle middle not necessary - that's us!
            if (startingPosition.x < numCols - 1)
            {
                adjacent.Add(grid[startingPosition.x + 1, startingPosition.y]); //middle right
            }
        }
        if (startingPosition.y >0)//Bottom row
        {
            if (includeCorners && startingPosition.x > 0)
            {
                adjacent.Add(grid[startingPosition.x - 1, startingPosition.y - 1]); //bottom right
            }
            adjacent.Add(grid[startingPosition.x, startingPosition.y - 1]); //bottom middle
            if (includeCorners && startingPosition.x < numCols - 1)
            {
                adjacent.Add(grid[startingPosition.x + 1, startingPosition.y - 1]); //bottom right
            }
        }
        return adjacent;
    }
    /// <summary>
    /// calls findAdjacent() with includeCorners set to FALSE
    ///  <para>returns List of adjacent cells left to right and top to bottom </para>
    /// </summary>
    /// <param name="startingPosition"></param>
    /// <returns></returns>
    public List<Cell> findAdjacent(Vector2Int startingPosition)
    {
        return findAdjacent(startingPosition, false);
    }



}