using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{


    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public enum GameState
    {
        Playing,
        Paused,
        Dead
    }

    public GameState gameState = GameState.Playing;

    public UnityEvent gamePauseEvent;
    public UnityEvent deathEvent;

    private GridManager gridManager;
    private ScoreManager scoreManager;

    private void Awake()
    {
        //is this the first time we've created this singleton
        if (_instance == null)
        {
            //we're the first GameManager, so assign ourselves to this instance
            _instance = this;
        }
        else
        {
            //if there's another one, then destroy this one
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gridManager = GridManager._instance;
        scoreManager = ScoreManager._instance;


        deathEvent.AddListener(gmDeathFunction);

    }

    public void resetGame()
    {
        gridManager.resetGrid();
        scoreManager.resetScore();

        gameState = GameState.Playing;

    }

    public void gmDeathFunction()
    {
        gameState = GameState.Dead;
    }
}
