using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightPool : MonoBehaviour
{

    private float lightSize;
    private Fungus parent;
    private float lightRadius;

    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponentInParent<Fungus>();

        //lightSize = 10;
        lightSize = parent.LightPoolRadius;

        lightRadius = (lightSize+ .5f) * 2; 

        Debug.Log(lightSize);

        this.transform.localScale = this.transform.localScale * lightRadius;

    }

    // Update is called once per frame
    void Update()
    {


    }
}
